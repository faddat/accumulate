// Package exp is experimental and may change without warning.
package exp

import (
	"gitlab.com/accumulatenetwork/accumulate/internal/core"
	"gitlab.com/accumulatenetwork/accumulate/internal/database/smt/managed"
)

type GlobalValues = core.GlobalValues

type Receipt = managed.Receipt
type ReceiptEntry = managed.ReceiptEntry
